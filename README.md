Informe Desarrollo frontend
============================

Descripción del Informe
-----------------------

El presente informe tiene que ver con la primera parte el proyecto desarrollo frontend que estara en desarrollo posteriormente. Se estara utilizando el famoso framework de javascript react.js con el motivo de crear una lading page sobre  un portal educativo para los estudiantes del Instituto Universitario Jesús Obrero,donde los temas abarcaran la programación orientada a objetos y el desarrollo web.

En esta  etapa se hará el levantamiento de la información,ciclo de vida del software, uso wireframes y mockups para a si lograr los objetivos bien logrados.Se estara usando herrramientas web como html, css, y javascript planos, en donde la información se estara plasmando la visión del proyecto funcionalidades y otros aspectos necesarios para documentar nuestro proyecto.

Instalación
-----------
Para obtener este informe se necesita tener conocimientos basicos de git y gitlab.

Se puede descargar mediante un archivo zip mediante en repositorio Desarrollo Frontend parate 1  y dirjirse a la rama Moreno_Christian para descargar.Tambien se puede hacer uso de git para clonar el repositorio mediante los siguientes comandos:

SHH -> git clone git@gitlab.com:iw-2021-1/desarrollo-frontend-parte-1.git

HTTPS -> git clone https://gitlab.com/iw-2021-1/desarrollo-frontend-parte-1.git
